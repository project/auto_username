<?php

namespace Drupal\auto_username;

/**
 * Defines options for handling punctuation in generated usernames.
 */
class AutoUsernamePunctuationOptions {
  const REMOVE = 0;
  const REPLACE = 1;
  const DO_NOTHING = 2;

}
