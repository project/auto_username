<?php

namespace Drupal\auto_username;

/**
 * Defines options for handling the case in generated usernames.
 */
class AutoUsernameOptions {
  const CASE_LEAVE_AS_IS = 0;
  const CASE_LOWER = 1;

}
